package com.infosys.infytel.friend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FriendFamilyApplication extends SpringBootServletInitializer{
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application){
		return application.sources(FriendFamilyApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(FriendFamilyApplication.class, args);
	}
}
